#! /usr/bin/env bash

function run_tests {
    pytest; RETVAL=$?
    return $RETVAL
}

run_tests
