from flask import Blueprint, jsonify


blueprint = Blueprint("home", __name__, url_prefix="/")

@blueprint.route("/", methods=["GET"])
def homepage():
    return jsonify({"message": "Hello, World!"})
