from os import environ as env

from flask import Flask

from views.home import blueprint as home


def start():
    flaskapp = Flask(env.get("FLASK_APP_NAME", __name__))
    flaskapp.register_blueprint(home)
    return flaskapp
