#! /usr/bin/env bash

function start_server {
    gunicorn --bind="${FLASK_RUN_HOST}:${FLASK_RUN_PORT}" "$@" "${FLASK_APP}"; RETVAL=$?
    return $RETVAL
}

start_server
