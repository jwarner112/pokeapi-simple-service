#! /usr/bin/env bash

declare -r USAGE="usage: $0 {build|serve|test}"
declare -ri ERR_USER=2
declare -ri ERR_PROG=1


function errmsg {
    echo "${USAGE}" >&2 && exit ${ERR_USER}
}


# TODO: move to env file
declare -r SERVE_FROM_PORT=4112  # will connect to a random port on the host


function build_docker_image {
  sudo docker build \
    --file "Dockerfile.flaskapp" \
    --tag "flaskapp-template:wip" \
    $(git rev-parse --show-toplevel)
}


function serve_flask_app {
  build_docker_image
  sudo docker run \
    --interactive \
    --tty \
    --publish ${SERVE_FROM_PORT} \
    --rm \
    --name "flaskapp" \
    "flaskapp-template:wip"
  RETVAL=$?
  return $RETVAL
}


function test_flask_app {
  build_docker_image
  sudo docker run \
    --interactive \
    --tty \
    --publish ${SERVE_FROM_PORT} \
    --rm \
    --name "flaskapp" \
    "flaskapp-template:wip" \
    "./test.sh"
  RETVAL=$?
  return $RETVAL

}


function main {
  [[ $# -ne 1 ]] && errmsg
  case $1 in
    "build")
      build_docker_image
      RESULT=$?
      ;;
    "serve")
      serve_flask_app
      RESULT=$?
      ;;
    "test")
      test_flask_app
      RESULT=$?
      ;;
    "*")
      errmsg
    ;;
  esac
  exit $RESULT
}

main "$@"